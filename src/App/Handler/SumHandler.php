<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\JsonResponse;

class SumHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $digitsToSum = [];

        foreach($request->getQueryParams() as $digitQueue) {
            $digit = $digitQueue;
            $digit = htmlspecialchars($digitQueue, ENT_HTML5, 'UTF-8');
            $digitsToSum[] = $digit;
        }

        return new JsonResponse([
            'total_sum' => array_sum($digitsToSum)
        ]);
    }
}
